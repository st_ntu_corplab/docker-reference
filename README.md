#Pull a prebuilt docker images
Go to https://hub.docker.com/explore/ to find a repository and take down the repositry name and tag name
Command: docker pull repositry:tag
For example: docker pull ubuntu:16.04

#Create a docker image
1. Create a Dockfile file with the content below:
FROM ubuntu:16.04

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y sudo \
	gedit \
	--no-install-recommends ubuntu-desktop \
	build-essential 

CMD ["/bin/bash"]

#Dockerfile reference
https://docs.docker.com/engine/reference/builder/#impact-on-build-caching

2. Build an image
sudo docker build . opt[-t repositry:tag]


#Run a docker image without GUI
Command: sudo docker run --rm -it repository:tag
note:
--rm 			: remove the container after it exit
-a=[]           : Attach to `STDIN`, `STDOUT` and/or `STDERR`
-t              : Allocate a pseudo-tty
--sig-proxy=true: Proxy all received signals to the process (non-TTY mode only)
-i              : Keep STDIN open even if not attached
For example: sudo docker run --rm -it ubuntu:16.04


#Run a docker image with GUI
1. Disable access control
In the current terminal type:
xhost +


2. Execute the command
sudo docker run --rm -it -e DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix repository:tag opt[app]
for example: sudo docker run --rm -it -e DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix repository:tag foo
note:
-v              : mount the directory
-e				: copy the environment variable from host to docker container

#Using Nvidia GPU
docker run --runtime=nvidia -ti --rm -e DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix -v ~/workspace:/workspace repository:tag opt[app]

#List Docker images
docker images

#List Docker container
docker ps -a


#Commit a container
docker commit container_id repository:tag


#Delete docker images
sudo docker rmi repository:tag

#Delete docker containers
sudo docker rm container_id

#Save a docker image to a file
docker save image > image.tar

#Load a file to docke as an image
docker load < input_file.tar

#Install Qt into docker
1. Run the container and execute the command below:
sudo add-apt-repository ppa:graphics-drivers/ppa
sudo apt-get update
sudo apt-get install nvidia-387





